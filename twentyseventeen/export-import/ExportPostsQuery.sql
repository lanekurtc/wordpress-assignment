/* Script to export Wordpress posts and post tag/category data within 3 month date range (including current month)
 *
 * Export Instructions:
 * - Run individual scripts below on desired Wordpress database in phpMyAdmin
 * - Click "Export" button below data desplayed
 * - Make sure "Dump all rows" under the "Rows" category is checked
 * - If importing into Wordpress install where tables with the same names already exist;
 *   - Click the "Custom" radio button under "Export Method"
 *   - Make sure the "Add create table statement" box under "Object creation options" is unchecked
 * - Click "Go"
 * - Copy and save the resulting query as an SQL file
 *
 * Import Instructions:
 * - Click "Import" tab on new Wordpress database installation in phpMyAdmin  
 * - Choose the files saved during the export and import each one
 */

SELECT wp_posts.* FROM wp_posts
WHERE post_date >= NOW() - INTERVAL 2 MONTH
AND post_date < NOW() + INTERVAL 1 DAY;

SELECT wp_postmeta.* FROM wp_postmeta
JOIN wp_posts ON wp_posts.ID = wp_postmeta.post_id
WHERE wp_posts.post_date >= NOW() - INTERVAL 2 MONTH
AND wp_posts.post_date < NOW() + INTERVAL 1 DAY;

SELECT wp_term_relationships.* FROM wp_term_relationships
JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id
WHERE wp_posts.post_date >= NOW() - INTERVAL 2 MONTH
AND wp_posts.post_date < NOW() + INTERVAL 1 DAY;

SELECT wp_term_taxonomy.* FROM wp_term_taxonomy
JOIN wp_term_relationships ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id
WHERE wp_posts.post_date >= NOW() - INTERVAL 2 MONTH
AND wp_posts.post_date < NOW() + INTERVAL 1 DAY;

SELECT wp_terms.* FROM wp_terms
JOIN wp_term_taxonomy ON wp_term_taxonomy.term_id = wp_terms.term_id
JOIN wp_term_relationships ON wp_term_relationships.term_taxonomy_id = wp_term_taxonomy.term_taxonomy_id
JOIN wp_posts ON wp_posts.ID = wp_term_relationships.object_id
WHERE wp_posts.post_date >= NOW() - INTERVAL 2 MONTH
AND wp_posts.post_date < NOW() + INTERVAL 1 DAY;